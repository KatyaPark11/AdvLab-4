﻿namespace AdvLab_4
{
    class ShowingTheDataBase
    {
        public static void Main()
        {
            while (true)
            {
                string dataBaseName = GetTheFileName();
                ConsoleHelper.ClearScreen();

                string dataBasePath = $"../../../../{dataBaseName}.csv";
                string schemePath = $"../../../../{dataBaseName}Scheme.json";

                if (!DoFileExists(dataBasePath, schemePath))
                {
                    ConsoleHelper.ClearScreen();
                    continue;
                }

                string[] dataBase = File.ReadAllLines(dataBasePath);
                Scheme scheme = Scheme.GetTheScheme(schemePath);

                DataExamination.IsAppropriateToSchema(dataBase, scheme);
                DataExamination.IsRightCountOfColumns(dataBase, scheme);

                var table = new Table(dataBase, scheme);

                DataWriting.OutputTheDatabase(table);
                ConsoleHelper.ClearScreen();

                Console.WriteLine("Do you want to exit?");
                if (AnswerForDecide()) break;
            }
        }

        private static string GetTheFileName()
        {
            Console.WriteLine("Enter the database name (The scheme of your file must have \"Scheme\" in the end of the name): ");
            string input = Console.ReadLine();

            while (input == null)
                input = Console.ReadLine();

            return input;
        }

        private static bool DoFileExists(string dataBasePath, string schemePath)
        {
            if (!File.Exists(dataBasePath) || !File.Exists(schemePath))
            {
                Console.CursorVisible = false;
                Console.WriteLine("This database doesn't exist. Check the database name and its scheme name.");
                Console.ReadKey();
                Console.CursorVisible = true;
                return false;
            }
            return true;
        }

        public static bool AnswerForDecide()
        {
            string answer = Console.ReadLine();
            return answer == "да" || answer == "yes";
        }
    }
}