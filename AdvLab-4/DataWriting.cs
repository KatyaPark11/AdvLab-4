﻿using System.Text;

namespace AdvLab_4
{
    class DataWriting
    {
        public static void OutputTheDatabase(Table table)
        {
            var rows = new StringBuilder();

            rows.Append(CreateTheTableHead(table));
            foreach (TableRow row in table.Rows)
            {
                rows.Append(CreateTheLineOfData(row, table.Scheme, table.MaxLens));
            }

            Console.WriteLine(rows);
            Console.ReadKey();
        }

        private static StringBuilder CreateTheLineOfData(TableRow row, Scheme scheme, List<int> maxLens)
        {
            var data = new List<string>();

            for (int i = 0; i < scheme.Columns.Count; i++)
            {
                data.Add(row.Data[scheme.Columns[i]].ToString().PadRight(maxLens[i]));
            }

            return CreateTheLineOfElements(data);
        }

        private static StringBuilder CreateTheTableHead(Table table)
        {
            var separators = new List<string>();
            var headers = new List<string>();
            var tableHead = new StringBuilder();

            Scheme scheme = table.Scheme;
            List<int> maxLens = table.MaxLens;

            for (int i = 0; i < scheme.Columns.Count; i++)
            {
                headers.Add(scheme.Columns[i].Name.PadRight(maxLens[i]));
            }

            foreach (var header in headers)
            {
                separators.Add(new string('-', header.Length));
            }

            tableHead.Append(CreateTheLineOfElements(headers));
            tableHead.Append(CreateTheLineOfElements(separators));

            return tableHead;
        }


        private static StringBuilder CreateTheLineOfElements(List<string> elements)
        {
            var line = new StringBuilder();

            foreach (var element in elements)
            {
                line.Append($"|{element}");
            }
            line.Append("|\n");

            return line;
        }
    }
}