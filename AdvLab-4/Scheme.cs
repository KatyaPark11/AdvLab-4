﻿using Newtonsoft.Json;

namespace AdvLab_4
{
    class Scheme
    {
        [JsonProperty(PropertyName = "name")]
        public string Name { get; private set; }

        [JsonProperty(PropertyName = "columns")]
        public List<SchemeColumn> Columns = new();

        public static Scheme GetTheScheme(string path)
        {
            return JsonConvert.DeserializeObject<Scheme>(File.ReadAllText(path));
        }
    }
}