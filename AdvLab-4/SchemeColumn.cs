﻿using Newtonsoft.Json;

namespace AdvLab_4
{
    class SchemeColumn
    {
        [JsonProperty(PropertyName = "name")]
        public string Name { get; private set; }

        [JsonProperty(PropertyName = "type")]
        public string Type { get; private set; }
    }
}