﻿namespace AdvLab_4
{
    class DataExamination
    {
        public static void IsAppropriateToSchema(string[] lines, Scheme scheme)
        {
            for (int i = 0; i < lines.Length; i++)
            {
                string[] elements = lines[i].Split(";");
                for (int j = 0; j < elements.Length; j++)
                {
                    string element = elements[j];
                    string type = scheme.Columns[j].Type;
                    bool res = true;

                    switch (type)
                    {
                        case "int":
                            res = Int32.TryParse(element, out _);
                            break;
                        case "bool":
                            res = bool.TryParse(elements[j], out _);
                            break;
                        case "DateTime":
                            res = DateTime.TryParse(elements[j], out _);
                            break;
                    }

                    if (!res) OutputAnErrorMessage(i + 1, j + 1, element, type);
                }
            }
        }

        public static void IsRightCountOfColumns(string[] lines, Scheme scheme)
        {
            for (int i = 0; i < lines.Length; i++)
            {
                string[] elements = lines[i].Split(";");
                int elementsCount = elements.Length;
                int rightCount = scheme.Columns.Count;

                if (elementsCount != rightCount) OutputAnErrorMessage(i + 1, rightCount, elementsCount);
            }
        }

        private static void OutputAnErrorMessage(int row, int column, string element, string type)
        {
            throw new Exception($"There is an error in {row} row and {column} column: Cannot convert \"{element}\" to \"{type}\".");
        }

        private static void OutputAnErrorMessage(int row, int rightCount, int wrongCount)
        {
            throw new Exception($"There is an error in {row} row:  The count of elements must be {rightCount} but it was {wrongCount}.");
        }
    }
}