﻿namespace AdvLab_4
{
    class Table
    {
        public string Name { get; private set; }
        public List<TableRow> Rows { get; private set; }
        public Scheme Scheme { get; private set; }
        public List<int> MaxLens { get; private set; } = new();

        public Table(string[] dataBase, Scheme scheme)
        {
            Name = scheme.Name;
            Rows = GetTheRows(dataBase, scheme);
            Scheme = scheme;

            GetTheStartLens();
            GetTheMaxLens();
        }

        private static List<TableRow> GetTheRows(string[] dataBase, Scheme scheme)
        {
            var rows = new List<TableRow>();

            for (int i = 0; i < dataBase.Length; i++)
            {
                string[] lineElements = dataBase[i].Split(";");

                var row = new TableRow();

                for (int j = 0; j < lineElements.Length; j++)
                {
                    row.Data.Add(scheme.Columns[j], GetTheValue(lineElements[j], scheme.Columns[j]));
                }

                rows.Add(row);
            }

            return rows;
        }

        private static object GetTheValue(string value, SchemeColumn column)
        {
            switch (column.Type)
            {
                case "int":
                    return int.Parse(value);
                case "float":
                    return float.Parse(value);
                case "bool":
                    return bool.Parse(value);
                case "DateTime":
                    return DateTime.Parse(value);
                default:
                    return value;
            }
        }

        private void GetTheStartLens()
        {
            foreach (SchemeColumn column in Scheme.Columns)
            {
                MaxLens.Add(column.Name.Length);
            }
        }

        private void GetTheMaxLens()
        {
            foreach (TableRow row in Rows)
            {
                for (int i = 0; i < row.Data.Count; i++)
                {
                    int elementLen = row.Data[Scheme.Columns[i]].ToString().Length;
                    if (elementLen > MaxLens[i])
                        MaxLens[i] = elementLen;
                }
            }
        }
    }
}