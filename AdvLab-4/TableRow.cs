﻿using System.Text;

namespace AdvLab_4
{
    class TableRow
    {
        public Dictionary<SchemeColumn, object> Data { get; private set; } = new();
    }
}